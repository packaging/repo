#!/usr/bin/env python3

import sys
import os
import os.path
from pydpkg import Dpkg
from urllib.parse import urlparse


def get_deb_packages(path):
    packages = []
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in [f for f in filenames if f.endswith(".deb")]:
            packages.append(os.path.join(dirpath, filename))
    return packages


def get_distros(repo):
    distros = []
    for distro in os.listdir(repo):
        if os.path.isdir(f"{repo}/{distro}"):
            distros.append(distro)
    return distros


def get_repos(path):
    repos = []
    for repo in os.listdir(path):
        if os.path.isdir(f"{path}/{repo}/pool"):
            repos.append(repo)
    return repos


def main(path, baseurl):
    if not os.path.exists("content"):
        os.makedirs("content")
    repos = get_repos(path)
    for repo in repos:
        if not os.path.exists("content/" + repo):
            os.makedirs("content/" + repo)
        repo_index = open("content/" + repo + "/_index.md", "w")
        repo_index.write("+++\n")
        repo_index.write('title = "' + repo.capitalize() + '"\n')
        repo_index.write("+++\n")
        repo_index.write("## Available distributions\n")
        repo_index.write("\n")
        distros = get_distros(f"{path}/{repo}/dists")
        for distro in distros:
            repo_index.write("* " + distro.capitalize() + "\n")
            if not os.path.exists("content/" + repo + "/" + distro):
                os.makedirs("content/" + repo + "/" + distro)
            distro_index = open("content/" + repo + "/" + distro + "/_index.md", "w")
            distro_index.write("+++\n")
            distro_index.write('title = "' + distro.capitalize() + '"\n')
            distro_index.write("+++\n")
            distro_index.write("```bash\n")
            distro_index.write(
                'echo "deb [arch=amd64] '
                + baseurl
                + "/"
                + repo
                + " "
                + distro
                + ' main" | sudo tee /etc/apt/sources.list.d/'
                + urlparse(baseurl).netloc
                + "-"
                + repo
                + "-"
                + distro
                + ".list\n"
            )
            distro_index.write("```\n\n")
            distro_index.write("## Available packages\n")
            packages = get_deb_packages(path + "/" + repo + "/pool/" + distro)
            for package in packages:
                print("processing package {}...".format(package))
                dp = Dpkg(package)
                distro_index.write("* " + dp.package + "\n")
                if not os.path.exists(
                    "content/" + repo + "/" + distro + "/" + dp.package
                ):
                    os.makedirs("content/" + repo + "/" + distro + "/" + dp.package)
                package_index = open(
                    "content/" + repo + "/" + distro + "/" + dp.package + "/_index.md",
                    "w",
                )
                package_index.write("+++\n")
                package_index.write('title = "' + dp.package + '"\n')
                package_index.write("+++\n")
                package_index.write(
                    '{{< button align="center" theme="info" href="'
                    + baseurl
                    + package.split(path)[1]
                    + '" >}} Download {{< /button >}}\n'
                )
                package_index.write("```\n")
                package_index.write(str(dp) + "\n")
                package_index.write("```\n")
                package_index.close()
            distro_index.close()
        repo_index.close()


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
