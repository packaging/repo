{{% panel theme="warning" header="2019-07-15: repo update" %}}
`suite` and `component` changed from nothing to real values, so apt will complain about an unsecure repo.
Just run an interactive `apt update` and accept the changes.
{{% /panel %}}

### Available repos

* Ubuntu (Bionic, Eoan, Focal)
* Debian (Buster)

### Add repo signing key to apt

```bash
sudo wget -q -O /etc/apt/trusted.gpg.d/morph027-repo.asc https://packaging.gitlab.io/repo/gpg.key
```

or

```bash
sudo curl -sLo /etc/apt/trusted.gpg.d/morph027-repo.asc https://packaging.gitlab.io/repo/gpg.key
```

### Add repo to apt

```bash
# echo "deb [arch=amd64] https://packaging.gitlab.io/repo/<available repo> <available repo distribution> main" | sudo tee /etc/apt/sources.list.d/<available repo>.list
```

Examples:

```bash
REPO=ubuntu-bionic; echo "deb [arch=amd64] https://packaging.gitlab.io/repo/${REPO%%-*} ${REPO##*-} main" | sudo tee /etc/apt/sources.list.d/morph027-${REPO}.list
```

```bash
REPO=debian-buster; echo "deb [arch=amd64] https://packaging.gitlab.io/repo/${REPO%%-*} ${REPO##*-} main" | sudo tee /etc/apt/sources.list.d/morph027-${REPO}.list
```
