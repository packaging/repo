#!/bin/bash

function error() {
    if [ -n "${INTERACTIVE}" ]; then
        sleep="${INTERACTIVE_SLEEP:-3600}"
        printf '\e[0;34minteractive debug mode enabled (%s)\e[0m\n' "${sleep}"
        sleep "${sleep}"
        printf '\e[0;34minteractive debug mode finished\n'
        exit 1
    fi
}

trap error ERR

apt-get -qq update
apt-get install -qqy apt-mirror apt-transport-https curl git python3-virtualenv
test -f venv/bin/activate || python3 -m virtualenv venv
venv/bin/pip3 install cryptography==37.0.4 pydpkg
curl -sLO https://github.com/gohugoio/hugo/releases/download/v"${HUGO}"/hugo_"${HUGO}"_Linux-64bit.deb
dpkg -i hugo_"${HUGO}"_Linux-64bit.deb
cp "${CI_PROJECT_DIR}"/mirror.list /etc/apt/mirror.list
apt-mirror
git clone https://github.com/vjeantet/hugo-theme-docdock themes/docdock
cat README.md >> content/_index.md
sed -i 's,^date.*,date = "'"$(date +%Y-%m-%dT%H:%M:%S%:z)"'",' content/_index.md
mkdir -p "${CI_PROJECT_DIR}"/public
cp -rv /var/spool/apt-mirror/mirror/minio.morph027.de/* "${CI_PROJECT_DIR}"/public/
venv/bin/python3 docdock.py public https://packaging.gitlab.io/repo
hugo
curl -L -o "${CI_PROJECT_DIR}"/public/gpg.key https://minio.morph027.de/ubuntu/gpg.key
